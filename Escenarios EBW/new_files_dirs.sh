#!/bin/bash

export LD_LIBRARY_PATH=/home/rich12/Escritorio/corrida_04/x86_64-linux-gnu:$LD_LIBRARY_PATH
#export LD_LIBRARY_PATH=/home/risolano/OndaCompleta_escenario_1/corrida_resto/x86_64-linux-gnu:$LD_LIBRARY_PATH

#Borrar enlace simbólico y archivo scr1 después de la corrida
#find ~/OndaCompleta_escenario_1/corrida_resto/final/ -type l -name "fullwave_input" -exec rm -rf {} +
#find ~/OndaCompleta_escenario_1/corrida_resto/final/ -type f -name "SCR1_camara" -exec rm -rf {} +


while read line
do
   myarr[$index]=$line
   index=$(($index+1))
done < myfile.txt

n=0
s=1 #no se cambia
k=$((20*n))
m=$((20*n+s))

mkdir final
for j in $(seq 1 1 2) #tres carpetas hasta la 51
do
	cd ~/Escritorio/corrida_04/final
	mkdir cor_$j
	for i in $(seq $k 1 $m)
	do  
		cd ~/Escritorio/corrida_04/final/cor_$j
		mkdir task_$i #crear carpetas para cada task
		cp ~/Escritorio/corrida_04/SCR1_camara task_$i #copiar el ejecutable en cada carpeta
		ln --symbolic ~/Escritorio/corrida_04/fullwave_input task_$i #enlace simbólico para que tome el input de una sola carpeta
		cd task_$i
		./SCR1_camara -s2 -p${myarr[3*i+1]} -t${myarr[3*i]} -w${myarr[3*i+2]} -T0 -i0 & #procesos 2 y 3 variables
	done
	k=$((m+1))
	m=$((k+s))
	#wait
	#find ~/Escritorio/corrida_04/final/cor_$j -type l -name "fullwave_input" -exec rm -rf {} +
	#find ~/Escritorio/corrida_04/final/cor_$j -type f -name "SCR1_camara" -exec rm -rf {} +
done
#wait
